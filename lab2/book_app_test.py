# -*- coding: utf-8 -*-

import os
import book_app
import unittest
import tempfile


class BookAppTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = book_app.app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client()
        # stworzenie tymczasowego pliku na bazę testową
        # i pobranie uchwytu do pliku oraz jego nazwy
        self.db_fd, self.app.config['DATABASE'] = tempfile.mkstemp()
        # inicjalizacja bazy - dodać kod
        book_app.init_db()

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        # zamknięcie tymczasowego pliku z bazą testową
        os.close(self.db_fd)
        # skasowanie tymczasowego pliku z bazą testową
        os.unlink(self.app.config['DATABASE'])

    def test_database_setup(self):
        """
        Testuje czy baza została poprawnie zainicjalizowana.
        """
        # dodać kod
        con = book_app.connect_db()
        # Pobranie informacji o tebeli entries
        cur = con.execute('PRAGMA table_info(books);')
        rows = cur.fetchall()
        # Tabela entries powinna mieć 3 kolumny
        self.assertEquals(len(rows), 5)


        database = open(self.app.config['DATABASE']).read()
        assert 'id text primary key' in database
        assert 'title text not null' in database
        assert 'author text not null' in database
        assert 'publisher text not null' in database
        assert 'isbn text not null' in database

    def test_get_all_books_empty(self):
        """
        Testuje brak książek w nowej bazie.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            data = book_app.get_books()
            assert len(data) == 0
            assert data == []

    def test_get_all_books(self):
        """
        Testuje odczyt z bazy wszystkich książek.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            book_app.populate_db()
            data = book_app.get_books()
            assert len(data) == 5

    def test_empty_listing(self):
        """
        Testuje odpowiedź strony głównej przy braku książek.
        """
        response = self.client.get('/')
        # dodać kod
        assert response.status == '404 NOT FOUND'
        assert '404 NOT FOUND' in response.data

    def test_listing(self):
        """
        Testuje odpowiedź strony głównej ze znanymi książkami.
        """
        # Symulacja żądania z URL = /
        with self.app.test_request_context('/'):
            # Wywołanie preprocesingu dekoratorów
            self.app.preprocess_request()
            # dodać kod
            book_app.populate_db()
            response = self.client.get('/')
            assert response.status == '200 OK'
            assert 'CherryPy Essentials: Rapid Python Web Application Development' in response.data
            assert 'Python for Software Design: How to Think Like a Computer Scientist' in response.data
            assert 'Foundations of Python Network Programming' in response.data
            assert 'Python Cookbook, Second Edition' in response.data
            assert 'The Pragmatic Programmer: From Journeyman to Master' in response.data
            assert '<a href="/book/id1/">' in response.data
            assert '<a href="/book/id2/">' in response.data
            assert '<a href="/book/id3/">' in response.data
            assert '<a href="/book/id4/">' in response.data
            assert '<a href="/book/id5/">' in response.data

    def test_details(self):
        """
        Testuje odpowiedź dla szczegółów konkretnej książki.
        """
        # dodać książki
        book_app.populate_db()
        response = self.client.get('/book/id3/')
        # dodać kod
        assert response.status == '200 OK'
        assert 'Foundations of Python Network Programming' in response.data
        assert 'John Goerzen' in response.data
        assert 'Apress; 2 edition (December 21, 2010)' in response.data
        assert '978-1430230038' in response.data

    def test_details_error(self):
        """
        Testuje odpowiedź dla szczegółów błędnej książki.
        """
        # dodać książki
        book_app.populate_db()
        response = self.client.get('/book/id33/')
        # dodać kod
        assert response.status == '404 NOT FOUND'
        assert '404 NOT FOUND' in response.data


if __name__ == '__main__':
    unittest.main()