# -*- coding: utf-8 -*-

import os
import sqlite3
from flask import Flask, g, render_template
import bookdb


app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'baza.db'),
    DEBUG=True,
    SECRET_KEY='admin',
    USERNAME='admin',
    PASSWORD='admin'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

book_db = bookdb.BookDB()


def connect_db():
    """Connects to the specific database."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db


@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()


def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()


def populate_db():
    with app.app_context():
        db = get_db()
        for book in book_db.titles():
            book_detail = book_db.title_info(book['id'])
            db.execute('INSERT INTO books (id, title, author, publisher, isbn) VALUES (?, ?, ?, ?, ?)',
                       [book['id'], book_detail['title'], book_detail['author'], book_detail['publisher'],
                        book_detail['isbn']])
        db.commit()


def get_books():
    db = get_db()
    cur = db.execute('SELECT * FROM books order by title')
    return cur.fetchall()


def get_book_detail(book_id):
    db = get_db()
    cur = db.execute('SELECT * FROM books WHERE id=?', [book_id])
    return cur.fetchall()


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    book_li = get_books()
    if book_li:
        return render_template('book_list.html', books=book_li)
    else:
        return render_template('404.html'), 404


@app.route('/book/<book_id>/')
def book(book_id):
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"
    book_det = get_book_detail(book_id)
    if book_det:
        return render_template('book_detail.html', book=book_det[0])
    else:
        return render_template('404.html'), 404

if __name__ == '__main__':
    init_db()
    populate_db()
    app.run(debug=True, host='194.29.175.240', port=22334)