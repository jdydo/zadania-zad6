drop table if exists books;
create table books (
  id text primary key,
  title text not null,
  author text not null,
  publisher text not null,
  isbn text not null
);