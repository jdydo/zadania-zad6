# -*- coding: utf-8 -*-

from flask import Flask, render_template
import bookdb

app = Flask(__name__)
app.config['DEBUG'] = True

db = bookdb.BookDB()


@app.route('/')
def books():
    # Przekaż listę książek do szablonu "book_list.html"
    return render_template('book_list.html', books=db.titles())


@app.route('/book/<book_id>/')
def book(book_id):
    # Przekaż szczegóły danej książki do szablonu "book_detail.html"
    try:
        return render_template('book_detail.html', book=db.title_info(book_id))
    except:
        return render_template('404.html'), 404

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=4444)