# -*- coding: utf-8 -*-

import os
import unittest
from tempfile import mkstemp
from blog_server import app_middleware
from webtest import TestApp
from bottle import install, uninstall
from bottle_sqlite import SQLitePlugin


class RouteTestCase(unittest.TestCase):

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.app = TestApp(app_middleware)
        self.db_fd, self.path = mkstemp()
        uninstall(SQLitePlugin)
        install(SQLitePlugin(dbfile=self.path))
        self.app.get('/reset')

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        self.app.reset()
        os.close(self.db_fd)
        os.unlink(self.path)
        uninstall(SQLitePlugin)
        install(SQLitePlugin(dbfile=os.path.dirname(os.path.realpath(__file__)) + '/blog.db'))

    ########################################################
    ### Testy routów serwera, wykorzystują moduł WebTest ###
    ########################################################

    def test_404(self):
        """
        Testuje reakcję na nieistniejącą stronę
        """
        assert self.app.get('/addaaa', status='*').status_int == 404

    def test_405(self):
        """
        Testuje reakcję na niedozwloną metodę
        """
        assert self.app.delete('/', status='*').status_int == 405
        assert self.app.put('/', status='*').status_int == 405
        assert self.app.post('/', status='*').status_int == 405

    def test_home_get(self):
        """
        Testuje odpowiedź serwera na zapytanie o strone główną z przykładowym postem
        """
        self.app.get('/add_sth')
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Zaloguj' in response
        assert 'Zarejestruj' in response
        assert 'Jan Kowalski' in response
        assert 'Lorem ipsum dolor sit amet' in response

    def test_login_get(self):
        """
        Testuje odpowiedź serwera na zapytanie o strone logowania
        """
        response = self.app.get('/login')
        assert response.status_int == 200
        assert 'Login' in response
        assert 'Hasło' in response
        assert 'name="login"' in response
        assert 'name="password"' in response

    def test_login_post_wrong_data(self):
        """
        Testuje odpowiedź serwera na zapytanie z błędnymi danymi logowania
        """
        response = self.app.post('/login', {'login': 'aa', 'password': 'bb'}, status='*')
        assert response.status_int == 401
        assert 'Nieprawidłowy login lub hasło' in response

    def test_login_post_good_data(self):
        """
        Testuje odpowiedź serwera na zapytanie z dobrymi danymi logowania
        oraz zmiany po zalogowaniu
        """
        response = self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        assert response.status_int == 302
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Zalogowano jako Jan Kowalski' in response
        assert 'Dodaj wpis' in response
        assert 'Wyloguj' in response

    def test_logged_redirecting(self):
        """
        Testuje przekierowania po zalogowaniu
        """
        response = self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        assert response.status_int == 302
        response = self.app.get('/login')
        assert response.status_int == 302
        response = self.app.get('/register')
        assert response.status_int == 302

    def test_register_get(self):
        """
        Testuje odpowiedź serwera na zapytanie o strone rejestracji
        """
        response = self.app.get('/register')
        assert 'Login' in response
        assert 'Hasło' in response
        assert 'Imię' in response
        assert 'Nazwisko' in response
        assert 'name="login"' in response
        assert 'name="password"' in response
        assert 'name="first_name"' in response
        assert 'name="last_name"' in response

    def test_register_post_wrong_data1(self):
        """
        Testuje odpowiedź serwera na zapytanie o rejestracje użytkownika z nieunikalny loginem
        """
        response = self.app.post(
            '/register',
            {'login': 'admin', 'password': 'admin', 'first_name': 'Jan', 'last_name': 'Kowalski'}
        )
        assert response.status_int == 200
        assert 'Błąd! Podany login jest zajęty.' in response

    def test_register_post_wrong_data2(self):
        """
        Testuje odpowiedź serwera na zapytanie o rejestracje użytkownika przy nie uzupełnieniu wszystkich pól
        """
        response = self.app.post(
            '/register',
            {'login': '', 'password': '', 'first_name': '', 'last_name': ''}
        )
        assert response.status_int == 200
        assert 'Błąd! Wszystkie pola muszą być wypełnione.' in response

    def test_register_post_good_data(self):
        """
        Testuje odpowiedź serwera na zapytanie o rejestracje użytkownika z poprawnie uzupełnionymi polami
        """
        response = self.app.post(
            '/register',
            {'login': 'Loginek', 'password': 'Hasełko', 'first_name': 'Imionko', 'last_name': 'Nazwiszcze'}
        )
        assert response.status_int == 302
        assert '/ok' in response.headers['Location']
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Zalogowano jako Imionko Nazwiszcze' in response
        assert 'Dodaj wpis' in response
        assert 'Wyloguj' in response

    def test_add_get_401(self):
        """
        Testuje odpowiedź serwera na zapytanie o stronę dodawania wpisów bez zalogowania
        """
        response = self.app.get('/add', status='*')
        assert response.status_int == 401
        assert 'Login' in response
        assert 'Hasło' in response
        assert 'name="login"' in response
        assert 'name="password"' in response
        assert 'Błąd! Dostęp do tej strony wymaga zalogowania.' in response

    def test_add_get_200_login(self):
        """
        Testuje odpowiedź serwera na zapytanie o stronę dodawania wpisów po zalogowaniu
        """
        self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        response = self.app.get('/add', status='*')
        assert response.status_int == 200
        assert 'Wprowadź tytuł' in response
        assert 'Wprowadź treść' in response
        assert 'name="content"' in response
        assert 'name="title"' in response

    def test_add_get_401_logout(self):
        """
        Testuje odpowiedź serwera na zapytanie o stronę dodawania wpisów po wylogowanu
        """
        self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        self.app.get('/logout')
        response = self.app.get('/add', status='*')
        assert response.status_int == 401
        assert 'Login' in response
        assert 'Hasło' in response
        assert 'name="login"' in response
        assert 'name="password"' in response
        assert 'Błąd! Dostęp do tej strony wymaga zalogowania.' in response

    def test_add_post_wrong_data(self):
        """
        Testuje odpowiedź serwera na zapytanie o przesłanie wpisu z nieuzupełnionymi polami
        """
        self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        response = self.app.post('/add', {'title': '', 'content': ''})
        assert response.status_int == 200
        assert 'Wprowadź tytuł' in response
        assert 'Wprowadź treść' in response
        assert 'name="content"' in response
        assert 'name="title"' in response
        assert 'Błąd! Tytuł i treść posta nie mogą być puste.' in response

    def test_add_post_good_data(self):
        """
        Testuje odpowiedź serwera na zapytanie o przesłanie wpisu z uzupełnionymi polami
        """
        self.app.post('/login', {'login': 'admin', 'password': 'admin'})
        response = self.app.post('/add', {'title': 'Tytulik', 'content': 'Treściątko'})
        assert response.status_int == 302
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Zalogowano jako Jan Kowalski' in response
        assert 'Dodaj wpis' in response
        assert 'Wyloguj' in response
        assert 'Tytulik' in response
        assert 'Treściątko' in response

    def test_add_post_good_data_logged_out(self):
        """
        Testuje odpowiedź serwera na zapytanie o przesłanie wpisu z uzupełnionymi polami, ale bez zalogowania
        """
        response = self.app.post('/add', {'title': 'Tytulik', 'content': 'Treściątko'}, status='*')
        assert response.status_int == 401
        assert 'Login' in response
        assert 'Hasło' in response
        assert 'name="login"' in response
        assert 'name="password"' in response
        assert 'Błąd! Dostęp do tej strony wymaga zalogowania.' in response
        response = self.app.get('/')
        assert response.status_int == 200
        assert 'Zaloguj' in response
        assert 'Zarejestruj' in response
        assert 'Tytulik' not in response
        assert 'Treściątko' not in response


if __name__ == '__main__':
    unittest.main()
