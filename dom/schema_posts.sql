drop table if exists posts;
create table posts (
  id integer primary key autoincrement,
  author text not null,
  date text not null,
  title text not null,
  content text not null
);