# -*- coding: utf-8 -*-

LOGIN_CONTENT = '''
        Zalogowano jako %s!
        <a style="margin-left:20px" href="/add">Dodaj wpis</a><a style="margin-left:20px" href="/logout">Wyloguj</a>
        '''

LOGOUT_CONTENT = '''
        <a href="/login">Zaloguj</a><a style="margin-left:20px" href="/register">Zarejestruj</a>
        '''

EXAMPLE_CONTENT = '''
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis ante ut nibh ornare congue.
    Nam at lacus et sapien convallis viverra quis nec tellus. Curabitur aliquam metus purus, nec vestibulum elit
    egestas non. Donec sit amet augue vitae orci luctus consequat. Nam tincidunt libero quam, id molestie lacus
    hendrerit at. Integer posuere tempor nunc et dictum. Sed venenatis tincidunt tortor, viverra ultricies est lacinia
    vitae. Duis facilisis, lorem et congue interdum, diam ligula sollicitudin nulla, vitae posuere erat felis id mauris.
    Morbi fringilla quam nec nisi varius ultricies. Mauris vel varius libero, at sollicitudin mi. Pellentesque rhoncus
    quam nec sodales blandit. Maecenas dolor nibh, scelerisque in pellentesque blandit, iaculis in lectus.

    Phasellus mattis nisl erat, sit amet eleifend elit tristique eu. Maecenas sit amet massa augue. Proin ut velit sem.
    Interdum et malesuada fames ac ante ipsum primis in faucibus. Class aptent taciti sociosqu ad litora torquent per
    conubia nostra, per inceptos himenaeos. Ut nec accumsan velit, consectetur dignissim metus. Vestibulum ante ipsum
    primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed vel dolor nibh. Interdum et malesuada fames
    ac ante ipsum primis in faucibus.
    '''

EXAMPLE_TITLE = '''
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis ante ut nibh ornare congue.
    Nam at lacus et sapien convallis viverra quis nec tellus.
    '''