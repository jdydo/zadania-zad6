# -*- coding: utf-8 -*-

from bottle import run, request, error, route, redirect, install, template, default_app, static_file, response
from beaker.middleware import SessionMiddleware
from bottle_sqlite import SQLitePlugin
from blog_server_db_methods import *
from blog_content import *
import os


app = default_app()
install(SQLitePlugin(dbfile=os.path.dirname(os.path.realpath(__file__)) + '/blog.db'))

session_opts = {
    'session.type': 'file',
    'session.cookie_expires': 1800,
    'session.data_dir': './data',
    'session.auto': True
}
app_middleware = SessionMiddleware(app, session_opts)


@route('/reset', method='GET')
def blog(db):
    base_ini_users(db)
    base_ini_add_admin(db)
    base_ini_posts(db)
    redirect('/')


@route('/add_sth', method='GET')
def blog(db):
    base_ini_add_example_post(db)
    redirect('/')


@route('/ok', method='GET')
def blog():
    return template('messange.tpl', messange='Operacja wykonana!')


@route('/', method='GET')
def blog(db):
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        logged_content = LOGIN_CONTENT % logged
    else:
        logged_content = LOGOUT_CONTENT

    return template('show_posts.tpl', logged=logged_content, posts=get_posts(db))


@route('/static/style.css')
def send_static():
    return static_file('style.css', 'static')


@route('/logout', method='GET')
def logout():
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        session['logged'] = None
        session.save()
        redirect('/ok')
    else:
        redirect('/')


@route('/login', method='GET')
def login():
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        redirect('/')
    else:
        return template('login.tpl', error='')


@route('/login', method='POST')
def login(db):
    logged = check_user(db, request.forms.get('login'), request.forms.get('password'))

    if logged:
        session = request.environ.get('beaker.session')
        session['logged'] = logged
        session.save()
        redirect('/')
    else:
        response.status = '401 Unauthorized'
        return template('login.tpl', error='Nieprawidłowy login lub hasło')


@route('/register', method='GET')
def register():
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        redirect('/')
    else:
        return template('register.tpl', error='')


@route('/register', method='POST')
def register(db):
    login = request.forms.get('login')
    password = request.forms.get('password')
    first_name = request.forms.get('first_name')
    last_name = request.forms.get('last_name')

    if check_login(db, login):
        return template('register.tpl', error='Błąd! Podany login jest zajęty.')
    elif login and password and first_name and last_name:
        register_user(db, login, password, first_name, last_name)
        session = request.environ.get('beaker.session')
        session['logged'] = '%s %s' % (first_name, last_name)
        session.save()
        redirect('/ok')
    else:
        return template('register.tpl', error='Błąd! Wszystkie pola muszą być wypełnione.')


@route('/add', method='GET')
def add_post():
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        logged_content = LOGIN_CONTENT % logged
        return template('add_post.tpl', logged=logged_content, error='', input_text='', textarea='')
    else:
        response.status = '401 Unauthorized'
        return template('login.tpl', error='Błąd! Dostęp do tej strony wymaga zalogowania.')


@route('/add', method='POST')
def add_post(db):
    session = request.environ.get('beaker.session')
    logged = session.get('logged', None)

    if logged:
        title = request.forms.get('title')
        content = request.forms.get('content')
        logged_content = LOGIN_CONTENT % logged

        if title and content:
            send_post(db, logged, title, content)
            redirect('/ok')
        else:
            return template('add_post.tpl', logged=logged_content,
                            error='Błąd! Tytuł i treść posta nie mogą być puste.', input_text=title, textarea=content)
    else:
        response.status = '401 Unauthorized'
        return template('login.tpl', error='Błąd! Dostęp do tej strony wymaga zalogowania.')


@error(404)
def error404(error):
    return '<h1>404 Not Found<h1/>'


@error(405)
def error404(error):
    return '<h1>405 Method Not Allowed<h1/>'


if __name__ == "__main__":
    run(app_middleware, host='localhost', port=4444, reloader=True)
