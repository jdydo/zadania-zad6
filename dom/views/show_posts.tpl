<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Mikroblog</title>
    <link href="/static/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main">
    <div class="menu">{{ !logged }}</div>
    % for post in posts:
    <div class="post">
        <div class="header">
            <div class="title">
                <strong>{{ post['title'] }}</strong>
            </div>
            <div class="author">
                {{ post['author'] }} - {{ post['date'] }}
            </div>
        </div>
        <hr>
        <div class="content">
            {{ post['content'] }}
        </div>
    </div>
    % end
</div>
</body>
</html>