<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Mikroblog</title>
    <link href="/static/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<form class="login" action="/login" method="POST">
    <table style="width: 300px; margin-right: auto; margin-left: auto">
        <tr>
            <td style="padding:10px; text-align:right;">Login:</td>
            <td><input name="login" type="text"/></td>
        </tr>
        <tr>
            <td style="padding:10px; text-align:right;">Hasło:</td>
            <td><input name="password" type="password"/></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="Zaloguj"/></td>
        </tr>

    </table>
    <p style="color: red"><strong>{{ error }}</strong></p>
</form>
</body>
</html>