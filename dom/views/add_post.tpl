<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Mikroblog</title>
    <link href="/static/style.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="main">
    <div class="menu">{{ !logged }}</div>
    <div class="post">
        <a style="float:right" href="/">Wróć do strony głównej</a>

        <form action="/add" method="POST">
            <div style="margin-bottom: 20px;">
                <label>Wprowadź tytuł: </label><input style="width: 400px; margin-left: 10px" name="title" type="text"
                                                      value="{{ input_text }}"/>
            </div>

            <div>
                <div style="margin-bottom: 10px">Wprowadź treść:</div>
                <textarea name="content" rows="4" cols="50">{{ textarea }}</textarea>
            </div>

            <input style="width: 75px; height: 50px" type="submit" value="Wyslij"/>
        </form>
        <p style="color: red"><strong>{{ error }}</strong></p>
    </div>
</div>
</body>
</html>