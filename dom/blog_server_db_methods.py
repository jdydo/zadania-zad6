# -*- coding: utf-8 -*-

import time
from blog_content import *


def base_ini_posts(db):
    schema_posts = open('schema_posts.sql', 'r').read()
    db.executescript(schema_posts)


def base_ini_users(db):
    schema_users = open('schema_users.sql', 'r').read()
    db.executescript(schema_users)


def base_ini_add_admin(db):
    db.execute(
        'INSERT INTO users (login, password, first_name, last_name) VALUES (?, ?, ?, ?)',
        [
            'admin',
            'admin',
            'Jan',
            'Kowalski'
        ]
    )
    db.commit()


def base_ini_add_example_post(db):
    content = EXAMPLE_CONTENT
    title = EXAMPLE_TITLE
    send_post(db, 'Jan Kowalski', title, content)


def get_posts(db):
    return db.execute(
        'SELECT * FROM posts order by id DESC'
    ).fetchall()


def send_post(db, author, title, content):
    db.execute(
        'INSERT INTO posts (author, date, title, content) VALUES (?, ?, ?, ?)',
        [
            author.decode('utf-8'),
            time.strftime("%d/%m/%Y  %H:%M:%S").decode('utf-8'),
            title.decode('utf-8'),
            content.decode('utf-8')
        ]
    )
    db.commit()


def register_user(db, login, password, first_name, last_name):
    db.execute(
        'INSERT INTO users (login, password, first_name, last_name) VALUES (?, ?, ?, ?)',
        [
            login.decode('utf-8'),
            password.decode('utf-8'),
            first_name.decode('utf-8'),
            last_name.decode('utf-8')
        ]
    )
    db.commit()


def check_user(db, login, password):
    result = db.execute(
        'SELECT first_name, last_name FROM users WHERE login = ? AND password = ?',
        [
            login.decode('utf-8'),
            password.decode('utf-8')
        ]
    ).fetchall()
    if result:
        return '%s %s' % (result[0]['first_name'].decode('utf-8'), result[0]['last_name'].decode('utf-8'))
    else:
        return None


def check_login(db, login):
    result = db.execute(
        'SELECT login FROM users WHERE login = ?',
        [
            login.decode('utf-8')
        ]
    ).fetchall()
    if result:
        return True
    else:
        return False