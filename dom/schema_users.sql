drop table if exists users;
create table users (
  id integer primary key autoincrement,
  login text not null,
  password text not null,
  first_name text not null,
  last_name text not null
);