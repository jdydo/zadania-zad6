# -*- coding: utf-8 -*-

import os
import unittest
from sqlite3 import connect, Row
from tempfile import mkstemp
from blog_server_db_methods import *


class DbTestCase(unittest.TestCase):
    def connect_db(self):
        db = connect(self.path)
        db.row_factory = Row
        return db

    def setUp(self):
        """
        Metoda wywoływana przed uruchomieniem każdego testu.
        Ustawia aplikację do testów i tworzy testową bazę danych.
        """
        self.db_fd, self.path = mkstemp()
        self.db = self.connect_db()
        base_ini_posts(self.db)
        base_ini_users(self.db)

    def tearDown(self):
        """
        Metoda uruchamiana po zakończeniu każdego testu.
        Czyści testową bazę danych.
        """
        self.db.close()
        os.close(self.db_fd)
        os.unlink(self.path)

    ###################################
    ### Testy metod dostępu do bazy ###
    ###################################

    def test_database_setup_posts(self):
        """
        Testuje czy baza posts została poprawnie zainicjalizowana.
        """
        result = self.db.execute('PRAGMA table_info(posts);').fetchall()
        self.assertEquals(len(result), 5)

    def test_database_setup_users(self):
        """
        Testuje czy baza users została poprawnie zainicjalizowana.
        """
        result = self.db.execute('PRAGMA table_info(users);').fetchall()
        self.assertEquals(len(result), 5)

    def test_get_all_posts_empty(self):
        """
        Testuje brak wpisów w nowej bazie.
        """
        data = get_posts(self.db)
        assert len(data) == 0
        assert data == []

    def test_get_example_post(self):
        """
        Testuje odczyt z bazy przykładowego wpisu.
        """
        base_ini_add_example_post(self.db)
        data = get_posts(self.db)
        assert len(data) == 1
        assert data[0]['id']
        assert data[0]['title']
        assert data[0]['date']
        assert data[0]['content']
        assert data[0]['author']

    def test_send_example_post(self):
        """
        Testuje wysyłanie przykładowego wpisu.
        """
        send_post(self.db, 'test_author', 'test_title', 'test_content')
        data = get_posts(self.db)
        assert len(data) == 1
        assert data[0]['id']
        assert data[0]['date']
        assert data[0]['title'] == 'test_title'
        assert data[0]['content'] == 'test_content'
        assert data[0]['author'] == 'test_author'

    def test_empty_user(self):
        """
        Testuje brak konta administratora w nowej bazie.
        """
        data = check_user(self.db, 'admin', 'admin')
        assert data is None

    def test_get_user(self):
        """
        Testuje brak konta administratora w nowej bazie.
        """
        base_ini_add_admin(self.db)
        data = check_user(self.db, 'admin', 'admin')
        assert data == 'Jan Kowalski'

    def test_register_user(self):
        """
        Testuje rejetstracje konta w nowej bazie.
        """
        register_user(self.db, 'Janeczka', 'Hasełko', 'Imionko', 'Nazwiszcze')
        assert check_login(self.db, 'Janeczka')
        assert check_user(self.db, 'Janeczka', 'Hasełko') == 'Imionko Nazwiszcze'

    def test_get_user_login_unavailable(self):
        """
        Testuje brak dostępności loginu istniejącego w bazie
        """
        base_ini_add_admin(self.db)
        data = check_login(self.db, 'admin')
        assert data is True

    def test_get_user_login_available(self):
        """
        Testuje dostępność loginu nieistniejącego w bazie
        """
        base_ini_add_admin(self.db)
        data = check_login(self.db, 'Janeczka')
        assert data is False

if __name__ == '__main__':
    unittest.main()
